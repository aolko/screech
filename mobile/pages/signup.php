<?php
// Include config file
require_once "resources/connect.php";
 
// Define variables and initialize with empty values
$username = $password = $confirm_password = $fullname = $recaptcha_err = "";
$username_err = $password_err = $confirm_password_err = $fullname_err = $doesagree_err = "";

// Processing form data when form is submitted
if($_SERVER["REQUEST_METHOD"] == "POST"){
    // Validate username
    if(empty(trim($_POST["username"]))){
        $username_err = "Please enter a username.";
    } elseif(strlen(trim($_POST["password"])) > 20){
        $username_err = "Username cannot be over 20 characters.";
    } else{
        // Prepare a select statement
        $sql = "SELECT id FROM users WHERE username = ?";
        if($stmt = mysqli_prepare($conn, $sql)){
            // Bind variables to the prepared statement as parameters
            mysqli_stmt_bind_param($stmt, "s", $param_username);
            // Set parameters
            $param_username = $_POST["username"];
            // Attempt to execute the prepared statement
            if(mysqli_stmt_execute($stmt)){
                /* store result */
                mysqli_stmt_store_result($stmt);
                
                if(mysqli_stmt_num_rows($stmt) == 1){
                    $username_err = "This username is already taken.";
                } else{
                    $username = trim($_POST["username"]);
                }
            } else{
                echo "Oops! Something went wrong. Please try again later.";
            }
        }
        // Close statement
        mysqli_stmt_close($stmt);
    }
    // Validate password
    if(empty(trim($_POST["password"]))){
        $password_err = "Please enter a password.";     
    } elseif(strlen(trim($_POST["password"])) < 6){
        $password_err = "Password must have atleast 6 characters.";
    } else{
        $password = $_POST["password"];
    }
    // Validate confirm password
    if(empty(trim($_POST["confirm_password"]))){
        $confirm_password_err = "Please confirm password.";     
    } else{
        $confirm_password = trim($_POST["confirm_password"]);
        if(empty($password_err) && ($password != $confirm_password)){
            $confirm_password_err = "Password did not match.";
        }
    }
    // Validate display name
    if(empty(trim($_POST["fullname"]))){
        $fullname_err = "Please enter a display name.";     
    } else{
        $fullname = $_POST["fullname"];
    }

    // Check input errors before inserting in database
    if(empty($username_err) && empty($password_err) && empty($confirm_password_err) && empty($fullname_err)){
        
        // Prepare an insert statement
        $sql = "INSERT INTO users (username, password) VALUES (?, ?)";
         
        if($stmt = mysqli_prepare($conn, $sql)){
            $sql2 = "SELECT id, username, password, likes_replies FROM users WHERE username = ?";
            // Bind variables to the prepared statement as parameters
            mysqli_stmt_bind_param($stmt, "ss", $param_username, $param_password);
            
            // Set parameters
            $param_username = $username;
            $param_password = password_hash($password, PASSWORD_DEFAULT);
            
            
            // Attempt to execute the prepared statement
            if(mysqli_stmt_execute($stmt)){
                // Login
                if($stmt2 = mysqli_prepare($conn, $sql2)){
                    // Bind variables to the prepared statement as parameters
                    mysqli_stmt_bind_param($stmt2, "s", $param_username);
                    
                    // Attempt to execute the prepared statement
                    if(mysqli_stmt_execute($stmt2)){
                        // Store result
                        mysqli_stmt_store_result($stmt2);
                        
                        // Check if username exists, if yes then verify password
                        if(mysqli_stmt_num_rows($stmt2) == 1){                    
                            $stmt->bind_result($id, $db_username, $password, $likes_replies);
                            $stmt->fetch();                    
                            session_start();
                                    
                            // Store data in session variables
                            $_SESSION["loggedin"] = true;
                            //$_SESSION["id"] = $id;
                            $_SESSION["username"] = $db_username;
                            $_SESSION["likes_replies"] = $likes_replies;
                            header("Location: /");
                        }
                    }
                } else {
                echo "Couldn't login.";}
        } else{
                echo "Something went wrong. Please try again later.";
            }
        }
         
        // Close statement
        mysqli_stmt_close($stmt);
    }
    
    // Close connection
    mysqli_close($conn);
}
?>
    <form action="signup" method="post">
            <b>Create a Free Screech Account</b><br><br>
            <b>Display Name:</b><br>
                <input type="text" name="fullname" value="<?php echo $fullname; ?>">
                <span class="error"><?php echo $fullname_err; ?></span>
            <br>
            <b>Username</b><br>
                <input type="text" name="username" class="form-control" value="<?php echo $username; ?>">
                <span class="error"><?php echo $username_err; ?></span>  
            <br>
            <b>Password</b><br>
                <input type="password" name="password" class="form-control" value="<?php echo $password; ?>">
                <span class="error"><?php echo $password_err; ?></span>
            <br>
            <b>Confirm Password</b><br>
                <input type="password" name="confirm_password" class="form-control" value="<?php echo $confirm_password; ?>">
                <span class="error"><?php echo $confirm_password_err; ?></span>
            <br>
            <span>
                By clicking button below, you agree to <a href='/tos'>Screech's Terms of Service</a>
                </span><br>
            <input type="submit" class="btn btn-primary" value="Submit">
    </form>