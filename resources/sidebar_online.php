<?php 
    $sql = "SELECT * FROM users WHERE username = '$prof_user'";
    $followsql = "SELECT * FROM following WHERE (user1 = '$log_name' OR user1 = '$prof_user') AND (user2 = '$log_name' OR user2 = '$prof_user')";
    $followersql = "SELECT * FROM following WHERE user2 = '$prof_user'";
    $followingsql = "SELECT * FROM following WHERE user1 = '$prof_user'";
    $currenttweetsql = "SELECT * FROM tweets WHERE username = '$log_name' ORDER BY CAST(id as SIGNED INTEGER) DESC LIMIT 1";
    $bgattachment = $bgcover = 'initial';
    $bgrepeat = 'no-repeat';
    $bgposition1 = 'top';
    $bgposition2 = 'left';
    $result = mysqli_query($conn, $sql);
    $followresult = mysqli_query($conn, $followsql);
    $followerresult = mysqli_query($conn, $followersql);
    $followingresult = mysqli_query($conn, $followingsql);
    $currenttweetresult = mysqli_query($conn, $currenttweetsql);
    $currenttweetrow = mysqli_fetch_array($currenttweetresult);
    $followingNo = $followerNo = $isFollowing = 0;
    $followers = array();
    $followingNo = mysqli_num_rows($followingresult);
    if (!$result) {
        printf("Error: %s\n", mysqli_error($conn));
    }
    if (!$followresult) {
        printf("Follow error: %s\n", mysqli_error($conn));
    }
    if (!$followerresult) {
        printf("Follower error: %s\n", mysqli_error($conn));
    }
    if (!$followingresult) {
        printf("Following error: %s\n", mysqli_error($conn));
    }
    $row = mysqli_fetch_array($result);
    if (!$personalfavoriteresult) {
        printf("Personal favorites error: %s\n", mysqli_error($conn));
    }
    while($followrow = mysqli_fetch_assoc($followresult)) {   
        foreach ($followrow as $key=>$value) {
            if($followrow['user1'] == $log_name ) {
                if($followrow['user2'] == $prof_user ) {
                    $isFollowing = 1;
                }
            }
        }
    }
    while($followerrow = mysqli_fetch_assoc($followerresult)) {   
        foreach ($followerrow as $key=>$value) {
            if (strpos($value, $followerrow["user1"]) === false) {
                continue;
            }
            array_push($followers, $followerrow['user1']);
            $followerNo = $followerNo += 1;
        }
    }
	$loadSidebar = 1;
	if ($isProfile == 0) {
		$loadSidebar = 0;
	}
	if ($isStatus == "isStatus") {
		$loadSidebar = 1;
	}
    if ($loadSidebar == 0) {
        echo("
        <div class='msg'>

        <span style='line-height: 1.3em;'>
        Welcome, <br><span style='font-size: 16px; font-weight: 600;'><a href='/$log_name'>");
        if (empty($globaluserinf['fullname'])) {
            echo $log_name;
        } else {
            echo $globaluserinf['fullname'];
        }
        echo("</a></span>
        </span>
        </div>
        ");
        if ($row["banned"] == 1) {
            echo("<p style='color: red'>NOTE: You've been banned. You can appeal to the admins of Screech if you believe this was an error, or you'd like more clarification.</p>");
        }
        echo("
        <br>
        <b>Currently:</b> <em>".$currenttweetrow['tweet']."</em><br><br>
            <a href='/$log_name/favorites/'>$personalfavoriteno Favorites</a><br>
            $personalfollowerno Followers<br>
            $personalfollowingno Following<br>
            $personaltweetno Updates<br>
            </a> <span class='msgup'></span><br><br>");
            foreach ($personalfollowers as $key=>$value) {
                echo "
                    <a class='follower_list' href='$value'>
                        <img title='$value' width='24' height='24' src='/profiles/images/$value.png'>
                    </a>";
            }
    } else  {
        if($log_name !== $prof_user) {
            if($offlineonline == "online") {
                if($isFriends == 1) {
                    if ($isFollowing == 1) {
                        echo("<input type='submit' class='unfollow' value='Unfollow' name='unfollow'>");
                    }
                    if ($isFollowing == 0) {
                        echo("<input type='submit' class='follow' value='Follow' name='follow'>");
                    }
                }
            }
            if ($row["banned"] == 1 && $isStatus != "isStatus") {
                header ("Location: /banned?prfl=$prof_user");
            }
        }
        $fullname = $row["fullname"];
        echo("<div class='msg'>
            About <strong>$prof_user</strong>
            </div><div class='profile_content'>");
            if(!empty($row["fullname"])) {
                echo "Name: ".$row["fullname"]."<br>";
            }
            if(!empty($row["bio"])) {
                echo "Bio: ".htmlspecialchars(strip_tags($row["bio"]))."<br>";
            }
            if(!empty($row["location"])) {
                echo "Location: ".$row["location"]."<br>";
            }
            if(!empty($row["webpage"])) {
                $website = $row["webpage"];
                if(strpos($website, '://') === false) {
                    $website = "http://".$website;
                }
                echo "Web: <a href='".$website."'>".$row['webpage']."</a><br>";
            }
            if($_SESSION['likes_styles'] == 0 && isset($_SESSION["loggedin"])) {
                echo "";
            } else {
                echo "<style class='profstyle'>";
                if ($row['bgattachment'] == 1) {
                    $bgattachment = "fixed";
                }
                if ($row['bgcover'] == 1) {
                    $bgcover = "100% 100%";
                }
                if ($row['bgrepeat'] == 1) {
                    $bgrepeat = "repeat";
                }
                if ($row['bgrepeat'] == 2) {
                    $bgrepeat = "repeat-x";
                }
                if ($row['bgrepeat'] == 3) {
                    $bgrepeat = "repeat-y";
                }
                if ($row['bgposition1'] == 0) {
                    $bgposition1 = "top";
                }
                if ($row['bgposition1'] == 1) {
                    $bgposition1 = "center";
                }
                if ($row['bgposition1'] == 2) {
                    $bgposition1 = "bottom";
                }
                if ($row['bgposition2'] == 0) {
                    $bgposition2 = "left";
                }
                if ($row['bgposition2'] == 1) {
                    $bgposition2 = "center";
                }
                if ($row['bgposition2'] == 2) {
                    $bgposition2 = "right";
                }
                if(!empty($row["bgcolor"])) {
                    echo "body {background-color: #".$row["bgcolor"]."}";
                }
                if(!empty($row["bgimage"])) {
                    echo "body {background-image: url('".$row["bgimage"]."')!important; background-attachment: $bgattachment!important; background-repeat: $bgrepeat!important; background-position: $bgposition1 $bgposition2!important; background-size: $bgcover!important;}";
                }
				if(!empty($row["sitcolor"])) {
					echo "body {color: ".$row["sitcolor"]."}";
				}
                echo ".sidebar {background-color: #".$row['sibgcolor']."; border-color: #".$row['sibdcolor'].";} a {color: ".$row['silcolor']."}</style>";
            };
            echo("<br>
            <a href='/$prof_user/favorites/'>$favoriteno Favorites</a><br>
            $followingNo Following<br>
            $followerNo Followers<br>
            $tweetno_all Updates<br>");
            foreach ($followers as $key=>$value) {
                echo "
                    <a class='follower_list' href='/$value'>
                        <img title='$value' width='24' height='24' src='/profiles/images/$value.png'>
                    </a>";
            }
    }
    if ($_SESSION['likes_styles'] == 0 && isset($_SESSION["loggedin"])) {
        echo "<script>$('style').remove();</script>";
    }
    if ($isProfile == 1) {
        echo "</div>";
    }
?>
<?php echo $offlinediv; echo $logouttext;?>