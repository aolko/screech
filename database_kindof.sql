--
--
--
--
-- This DOES work, but it's very old (dated December 2018) and would require many modifications
-- to work. I haven't touched the databases for Screech in a month so I forgot the exact table layout. 
--
-- For safety reasons all data has been stripped from the database, including any users and anything that
-- was in the tables.
-- 
--
--
--
-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Dec 27, 2018 at 02:08 AM
-- Server version: 10.2.17-MariaDB
-- PHP Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `u952690596_scree`
--

--
-- Table structure for table `apps`
--

CREATE TABLE `apps` (
  `id` int(255) NOT NULL,
  `appname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `owner` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `client_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `client_secret` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permissions` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Table structure for table `favorites`
--

CREATE TABLE `favorites` (
  `user` varchar(255) NOT NULL,
  `tweetid` varchar(255) NOT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Table structure for table `following`
--

CREATE TABLE `following` (
  `id` int(255) NOT NULL,
  `user1` varchar(69) DEFAULT NULL,
  `user2` varchar(69) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `user1` varchar(69) COLLATE utf8_unicode_ci NOT NULL,
  `user2` varchar(59) COLLATE utf8_unicode_ci NOT NULL,
  `subject` varchar(10000) COLLATE utf8_unicode_ci NOT NULL,
  `message` varchar(10000) COLLATE utf8_unicode_ci NOT NULL,
  `time` timestamp NOT NULL DEFAULT current_timestamp(),
  `id` int(11) NOT NULL,
  `has_read` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Table structure for table `requests`
--

CREATE TABLE `requests` (
  `user1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `id` int(255) NOT NULL,
  `declined` int(255) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tweets`
--

CREATE TABLE `tweets` (
  `id` int(255) NOT NULL,
  `username` varchar(20) NOT NULL,
  `user_id` int(255) DEFAULT NULL,
  `tweet` varchar(140) NOT NULL,
  `timestamp` datetime(6) NOT NULL DEFAULT current_timestamp(6),
  `sentfrom` varchar(255) NOT NULL DEFAULT 'unknown',
  `tweet_read` int(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(255) NOT NULL,
  `username` varchar(20) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `password` varchar(255) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `private` int(11) NOT NULL DEFAULT 0,
  `followers` int(255) DEFAULT 0,
  `following` int(255) DEFAULT 0,
  `tweets` int(255) DEFAULT 0,
  `fullname` text NOT NULL,
  `bio` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `location` text NOT NULL,
  `webpage` text NOT NULL,
  `bgcolor` varchar(255) NOT NULL,
  `sibgcolor` varchar(255) NOT NULL DEFAULT 'e0ff92',
  `sibdcolor` varchar(255) NOT NULL DEFAULT '87bc44',
  `sitcolor` varchar(255) NOT NULL DEFAULT '000000',
  `silcolor` varchar(255) NOT NULL DEFAULT '#03c',
  `bgimage` varchar(255) NOT NULL,
  `bgattachment` int(11) NOT NULL,
  `bgcover` int(11) NOT NULL,
  `bgrepeat` int(11) NOT NULL,
  `bgposition1` int(255) NOT NULL,
  `bgposition2` int(255) NOT NULL,
  `datetime` datetime(6) NOT NULL DEFAULT current_timestamp(6),
  `banned` int(11) NOT NULL,
  `likes_replies` int(11) NOT NULL,
  `likes_styles` int(255) NOT NULL DEFAULT 1,
  `likes_snow` int(255) NOT NULL DEFAULT 1,
  `award_xp` int(255) NOT NULL DEFAULT 0,
  `award_ds` int(255) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `apps`
--
ALTER TABLE `apps`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `favorites`
--
ALTER TABLE `favorites`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `favorites_contraint` (`user`,`tweetid`);

--
-- Indexes for table `following`
--
ALTER TABLE `following`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `followers_contraint` (`user1`,`user2`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `requests`
--
ALTER TABLE `requests`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tweets`
--
ALTER TABLE `tweets`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `apps`
--
ALTER TABLE `apps`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `favorites`
--
ALTER TABLE `favorites`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=242;

--
-- AUTO_INCREMENT for table `following`
--
ALTER TABLE `following`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=207;

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT for table `requests`
--
ALTER TABLE `requests`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `tweets`
--
ALTER TABLE `tweets`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=510;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=115;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
