<?php
// Initialize the session
session_start();
// Check if the user is already logged in, if yes then redirect him to welcome page
if(isset($_SESSION["loggedin"]) && $_SESSION["loggedin"] === true){
    //header("location: /");
}
// Include config file
require_once "resources/connect.php";
// Define variables and initialize with empty values
$username = $password = $likes_replies = $likes_styles = "";
$username_err = $password_err = "";
// Processing form data when form is submitted
if($_SERVER["REQUEST_METHOD"] == "POST" || isset($_GET['username']) || isset($_GET['password'])) {
	if(isset($_GET['username'])) {
		$username = $_GET['username'];
	} else {
		$username = $_POST['username'];
	}
	if(isset($_GET['password'])) {
		$password = $_GET['password'];
	} else {
		$password = $_POST['password'];
	}
    // Check if username is empty
    if(empty(trim($username))){
        $username_err = "Please enter username.";
    } else{
        $username = trim($username);
    }
    // Check if password is empty
    if(empty(trim($password))){
        $password_err = "Please enter your password.";
    } else{
        $password = trim($password);
    }
    $userinf = mysqli_query($conn, "SELECT * FROM users WHERE username = '$username'");
    $userinfq = mysqli_fetch_assoc($userinf);
    if($userinfq["banned"] == 1) {
        $username_err = "This account has been banned.";
    }
    // Validate credentials
    if(empty($username_err) && empty($password_err)){
        // Prepare a select statement
        $sql = "SELECT id, username, password, likes_replies, likes_styles FROM users WHERE username = ?";
        if($stmt = mysqli_prepare($conn, $sql)){            // Bind variables to the prepared statement as parameters
            mysqli_stmt_bind_param($stmt, "s", $param_username);
            // Set parameters
            $param_username = $username;
            $param_likesreplies = $likes_replies;
            $param_likesstyles = $likes_styles;
            // Attempt to execute the prepared statement
            if(mysqli_stmt_execute($stmt)){
                // Store result
                mysqli_stmt_store_result($stmt);
                // Check if username exists, if yes then verify password
                if(mysqli_stmt_num_rows($stmt) == 1){                    
                    // Bind result variables
                    mysqli_stmt_bind_result($stmt, $id, $username, $hashed_password, $likes_replies, $likes_styles);
                    if(mysqli_stmt_fetch($stmt)){
                        if(password_verify($password, $hashed_password)){
                            // Password is correct, so start a new session
                            session_start();
                            // Store data in session variables
                            $_SESSION["loggedin"] = true;
                            $_SESSION["id"] = $id;
                            $_SESSION["username"] = $username;    
                            $_SESSION["likes_replies"] = $likes_replies;
                            $_SESSION["likes_styles"] = $likes_styles;
                            // Set the cookies
                            $rememberme = $_GET['rememberme'];
                            // Redirect user to welcome page
                            echo "<script>
                            window.localStorage.setItem(['username'], '$username');
                            window.localStorage.setItem(['password'], '$password');
                            window.location.href = '/';
                            </script>";

                            // Redirect user to welcome page
                            
                        } else{
                            // Display an error message if password is not valid
                            $password_err = "The password you entered was not valid.";
                        }
                    }
                } else{
                    // Display an error message if username doesn't exist
                    $username_err = "No account found with that username.";
                }
            } else{
                echo "Oops! Something went wrong. Please try again later.";
            }
        }
        // Close statement
        mysqli_stmt_close($stmt);
    }
    // Close connection
    mysqli_close($conn);
}
?>
    <form action="login" method='post'>
            <br>
            <label>Username</label>
                <input type="text" name="username" class="form-control" value="<?php echo $username; ?>">
                <span class="error"><?php echo $username_err; ?></span>  
            <br>
            <label>Password</label>
                <input type="password" name="password" class="form-control" value="<?php echo $password; ?>">
                <span class="error"><?php echo $password_err; ?></span>
            <br>
            <input type="submit" class="btn btn-primary" value="Submit">
    </form>
    <br><br><br><br><br>
    (Screech uses cookies. See the <a target='a_blank' href='http://www.screech.xyz/privacypolicy'>privacy policy</a> for more info.)