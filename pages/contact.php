<h2>Contact</h2>
<strong>PLEASE read the <a href='/help'>help manual</a> before contacting me as your question might be answered there.</strong>
<br><br>

<p>I, the admin, can be reached via the following sources:</p>

<a href="https://www.twitter.com/IoI_xD">Twitter</a><br>
<a href="https://discord.gg/invite/F3ymXuQ">Discord</a><br>

<p>You can also contact our userbase for help <a href="https://discord.gg/VDqwyxF">on our Discord server.</a>