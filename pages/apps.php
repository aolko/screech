<?php
$got = array();
if (!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true) {
    header("Location: /login?page=apps");
}
$authapps=array();
$authresult = mysqli_query($conn, "SELECT * FROM `auth` WHERE username = '$log_name'");
$auth = $_GET['auth'];
array_walk($authapps, function(&$x) {$x = "'$x'";});
while($authrow = mysqli_fetch_assoc($authresult)) {
    foreach ($authrow as $key=>$value) {
        if (strpos($value, $authrow["appname"]) === false) {
            continue;
        }
    if(in_array($authapps,$authrow['appname'])) {
        continue;
    }
        array_push($authapps,$authrow['appname']);
    }
}
	$app_create_err = "";
	function getRandomString($length = 8, $type = 1) {
		tryagain:
		if ($type == 1) {
			$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		} elseif ($type == 2) {
			$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		}
		$string = '';
		for ($i = 0; $i < $length; $i++) {
			$string .= $characters[mt_rand(0, strlen($characters) - 1)];
		}
		if($checkun = mysqli_query($GLOBALS['conn'], "SELECT * FROM `apps` WHERE client_secret = $string OR client_id = $string")) {
			goto tryagain;
		}
		return $string;
	}
    if($_SERVER["REQUEST_METHOD"] == "POST") {
        if (isset($_POST['app_create'])) {
			$perms = 0;
			if(strpos($_POST['app_name'], ' ') !== false) {
				$app_create_err = "App cannot contain any spaces.";
			}
			if(mysqli_num_rows(mysqli_query($GLOBALS['conn'], "SELECT * FROM `apps` WHERE appname = '".$_POST['app_name']."'")) >= 1) {
				$app_create_err = "An app with that name already exists.";
			}
			switch($_POST['app_create_select']) {
				case 'per_1':
					$perms = 1;
					break;
				case 'per_2':
					$perms = 2;
					break;
				case 'per_3':
					$perms = 3;
					break;
                default:
					$app_create_err = "You must select a permission type.";
					break;
			}
			if (empty($app_create_err)) {
				if(mysqli_query($conn, "INSERT INTO `apps` (appname, appdesc, appimg, applink, owner, client_id, client_secret, permissions) VALUES ('".mysqli_real_escape_string($conn, $_POST['app_name'])."', '".mysqli_real_escape_string($conn, $_POST['app_desc'])."', '".mysqli_real_escape_string($conn, $_POST['app_img'])."', '".mysqli_real_escape_string($conn, $_POST['app_link'])."', '$log_name', '".getRandomString(24, 1)."', '".getRandomString(16, 2)."', $perms)")) {
					header("Location: /apps");
				} else {
					echo mysqli_error($conn);
				}
			}
        }
        if (isset($_POST['authaccept'])) {
            $appresult = mysqli_query($conn, "SELECT * FROM `apps` WHERE client_id = '$auth'");
            $app = mysqli_fetch_assoc($appresult);
            if(mysqli_query($conn, "INSERT INTO `auth` (username, appname) VALUES ('$log_name', '".mysqli_real_escape_string($conn, $app['appname'])."')")) {
				header("Location: /apps");
			} else {
				echo mysqli_error($conn);
			}
        }
        if (isset($_POST['authdecline'])) {
            header("Location: /apps");
        }
    }
if (isset($auth)) {
    $appresult = mysqli_query($conn, "SELECT * FROM `apps` WHERE client_id = '$auth'");
    $app = mysqli_fetch_assoc($appresult);
    echo "<div class='auth_message'>
    ";
    if(mysqli_num_rows($appresult) == 0) {
        echo "Invalid client id.";
    } else {
        echo "
    <img style='float: right' width='100' src='".$app['appimg']."'>
    <h2>Do you want to autherize <b>".$app['appname']."?</b></h2>
    <form action='' method='post'>
        <input type='submit' name='authaccept' class='btn' value='Yes'> <input type='submit' name='authdecline' class='btn' value='No'>
    </form><br>
    <em>\"".$app['appdesc']."\"</em><br>
    <em> Wants permission to ";
        switch ($app["permissions"]) {
            case "0":
                echo "do nothing";
                break;
            case "1":
                echo "read your screeches";
                break;
            case "2":
                echo "read your screechs and post on your behalf";
                break;
            case "3":
                echo "read your screeches, post on your behalf, and read your direct messages";
                break;
        }
        echo "
        <br><br>
        <b>Be careful which apps you allow on your behalf.</b> I can't moderate what these apps do 24/7. If you're suspicious about what an app is doing to your profile, you can investigate what an app and other apps might be doing via the <a href='/logs'>logs page.</a></b>";
    } echo "
    </div><br>
    <hr>";
}
?>
<div class='table_contain'>
<h2>Make an app</h2>
<form method='post' action=''>
	<label>App name: </label><input name='app_name' type="text" value=<?php echo $_POST['app_name'];?>><br>
    <label>App description: </label><input name='app_desc' type="text" value=<?php echo $_POST['app_desc'];?>><br>
    <label>App image: </label><input name='app_img' type="text" placeholder="https://i.imgur.com/..."  value=<?php echo $_POST['app_img'];?>><br>
    <label>App link: </label><input name='app_link' type="text" value=<?php echo $_POST['app_link'];?>> <abbr title='When somebody screeches from your app, the words \"from yourapp\" will appear next to the post. \"yourapp\" will be replaced with link back to whatever url you put here.'>(?)</abbr><br> 
	<label>Permissions:</label>
	<select class='app_create_select' name="app_create_select">
		<option value='per_1'>Read</option>
		<option value='per_2'>Read & write</option>
		<option value='per_3'>Read, write, & access direct messages</option>
        <option value='no' selected disabled></option>
	</select><br>
	<input class='app_create' name='app_create' type='submit' value='Create'>
	<span class='error app_create_err'><?php echo $app_create_err;?></span><br>
	<em class='api_mes'></em>
</form>
<h2>Your apps</h2>
<table width='100%' border='3' cellpadding='1' class='api_table'>
	<thead>
	<tr>
	<th>App Name</th>
    <th>App Description</th>
	<th>Creator</th>
	<th>Client ID</th>
	<th>Client Secret</th>
	<th>Permissions</th>
    <th>App Image</th>
    <th>App Link</th>
    <th>Actions</th>
	</tr>
	</thead>
	<tbody>
	<?php
$appresult = mysqli_query($conn, "SELECT * FROM `apps` WHERE (owner = '$log_name' OR appname IN ('".implode("','", $authapps)."'))");
if(!$appresult) {
    print_r(mysqli_error($conn));
}
while($approw = mysqli_fetch_assoc($appresult)) {
    foreach ($approw as $key=>$value) {
        if (strpos($value, $approw["appname"]) === false) {
            continue;
        }
        if(in_array($approw["client_id"], $got)) {
            continue;
        }
    $got[] = $approw["client_id"];
        echo "<tr id='".$approw["appname"]."'>";
        if($approw['owner'] == $log_name) {
            echo "<td class='app_name'><input type='text' class='appname' value='".htmlspecialchars($approw["appname"])."'><br><small style='display: none!important;' class='appn'>".htmlspecialchars($approw["appname"])."</small></td>";
            echo "<td class='app_desc'><textarea type='text' rows='10' class='appdesc'>".htmlspecialchars($approw["appdesc"])."</textarea></td>";
        } else {
            echo "<td>".htmlspecialchars($approw["appname"])."</td>";
            echo "<td>".htmlspecialchars($approw["appdesc"])."</td>";
        }
        echo "
		<td class='app_owner'>".htmlspecialchars($approw["owner"])."</td>";
        if($approw['owner'] == $log_name) {
            echo "<td class='app_id'>".htmlspecialchars($approw["client_id"])."</td>
            <td><em class='secretget' id='".htmlspecialchars($approw['appname'])."'>Click to reveal.</em></td>";
        }
        else {
            echo "<td></td><td></td>";
        }
		echo"<td>".$approw["permissions"]."";
        switch ($approw["permissions"]) {
            case "0":
                echo " <em style='color: red;'>(None.)</em>";
                break;
            case "1":
                echo " <em>(Read)</em>";
                break;
            case "2":
                echo " <em>(Read and write)</em>";
                break;
            case "3":
                echo " <em>(Read, write, and access direct messages)</em>";
                break;
        }
        echo "
        </td>";
        if($approw['owner'] == $log_name) {
            echo "<td class='app_image'> <input type='text' class='appimg' value='".htmlspecialchars($approw["appimg"])."'>
            <img src='".htmlspecialchars($approw["appimg"])."'></td>
            <td class='app_link'> <input type='text' class='applink' value='".htmlspecialchars($approw["applink"])."'></td>";
        } else {
            echo "<td>
            <img src='".htmlspecialchars($approw["appimg"])."'></td>
            <td><a href='".htmlspecialchars($approw["applink"])."'>".htmlspecialchars($approw["applink"])."</a></td>";
        }
        if($approw['owner'] == $log_name) {
            echo "<td class='app_actions'>
            <input type='submit' class='app_update' value='Update'>
            <input type='submit' class='app_delete' value='Delete'>
            <input type='submit' class='app_regen' value='Regenerate'></td>
            ";
        }
        echo "
        </tr>
            <script>
            var appowner = \"".$approw['owner']."\" ;
            </script>
        ";
    }
}
	?>
	</tbody>
	</table>
    </div>
    <span class='app_err'></span>