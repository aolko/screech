<script>
    // JSColor
    jscolor.init();
    </script>
<?php
$olduser = $log_name;
// USERNAME/PASSWORD CHANGES
    if($_SERVER["REQUEST_METHOD"] == "POST") {
        $userinfq = mysqli_query($conn, "SELECT * FROM users WHERE username='$olduser'");
        $userinf = mysqli_fetch_assoc($userinfq);
        if (isset($_POST['userchange'])) {
            $newuser = $_POST['newuser'];
            $curpassuser = $_POST['curpassuser'];
            $passcheck = $userinf['password'];
            $newinfq = mysqli_query($conn, "SELECT * FROM users WHERE username='$newuser'");
            $newinfa = mysqli_num_rows($newinfq);
            if ($newinfa >= 1) {
                $userch_err = "This username is already taken.";
            }
            if (password_verify($curpassuser, $userinf['password'])) {
                echo "";
            } else {
                $userch_err = "Incorrect password.";
            }
            if(empty(trim($newuser))){
                $userch_err = "Please enter a username.";
            } elseif(strlen(trim($newuser)) > 20){
                $userch_err = "New username cannot be over 20 characters.";
            } elseif(strpos($newuser, ' ') !== false) {
                $userch_err = "New username cannot have spaces.";
            } elseif(strstr($newuser, '/')) {
                $userch_err = "New username cannot have slashes.";
            }
            if (empty($userch_err)) {
                $queries = array(
                "UPDATE favorites SET user='$newuser' WHERE user='$olduser'",
                "UPDATE following SET user1='$newuser' WHERE user1='$olduser'",
                "UPDATE following SET user2='$newuser' WHERE user2='$olduser'",
                "UPDATE messages SET user1='$newuser' WHERE user1='$olduser'",
                "UPDATE messages SET user2='$newuser' WHERE user2='$olduser'",
                "UPDATE tweets SET username='$newuser' WHERE username='$olduser'",
                "UPDATE users SET username='$newuser' WHERE username='$olduser'",
                "UPDATE apps SET owner='$newuser' WHERE owner='$olduser'",
                "UPDATE apps SET owner='$newuser' WHERE owner='$olduser'"
                );
                foreach ($queries as &$value) {
                    mysqli_query($conn,$value);
                }
                unset($value);
                $_SESSION['username'] = $newuser;
                $userch_err = "Username successfully changed.";
            }
        }
        if (isset($_POST['passchange'])) {
            $oldpass = $_POST['oldpass'];
            $newpass = $_POST['newpass'];
            $confirmnewpass = $_POST['confirmnewpass'];
            if (password_verify($oldpass, $userinf['password'])) {
                echo "";
            } else {
                $passch_err = "Old password is not correct.";
            }
            if ($newpass != $confirmnewpass) {
                $passch_err = "New passwords don't match.";
            }
            if (empty($passch_err)) {
                mysqli_query($conn,"UPDATE users SET password='".password_hash($newpass, PASSWORD_DEFAULT)."' WHERE username='$log_name'");
                $passch_err = "Password successfully changed.";
            }
        }
    }
//EVERYTHING ELSE
if (!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true) {
    header ("Location: /");
} else {
    echo "";
}
$sql = "SELECT * FROM users WHERE username = '$log_name'";
$result = mysqli_query($conn, $sql);
if (!$result) {
    printf("Error: %s\n", mysqli_error($conn));
}
$row = mysqli_fetch_array($result);
echo "
    <div class='profilechanger'>
    <form method='post' action='/resources/profilechange.php' enctype='multipart/form-data'>
    <h2>Profile settings</h2>
        <label>Full Name: </label><input name='fullname' value='".$row["fullname"]."'type='text'><br>
        <label>Bio: </label><textarea name='bio' rows='7' cols='50' type='text'>".$row["bio"]."</textarea><br>
        <label>Location: </label><input name='location' value='".$row["location"]."'type='text'><br>
        <label>Website: </label><input name='website' value='".$row["webpage"]."'type='text'><br>
        <label>Profile Picture: </label><input name='fileToUpload' type='file'><br><br>
        <label>Sidebar Background Color: </label><input class='jscolor' name='sibgcolor' type='text' value='".$row['sibgcolor']."' placeholder='#XXXXXX'><br>
        <label>Sidebar Border Color: </label><input class='jscolor' name='sibdcolor' type='text' value='".$row['sibdcolor']."' placeholder='#XXXXXX'><br><br>
        <label>Profile Text Color: </label><input class='jscolor' name='sitcolor' type='text' value='".$row['sitcolor']."' placeholder='#XXXXXX'><br>
		<label>Profile Link Color: </label><input class='jscolor' name='silcolor' type='text' value='".$row['silcolor']."' placeholder='#XXXXXX'><br>
        <span class='error'>NOTE: All images linked below must follow <a href='/tos'>Screech's Terms of Service.</a></span><br>
        <b>The background image must be a DIRECT link to the picture. Imgur is recommended.</b><br>
        <label>Background Image: </label><input type='text' name='bgimage' value='".$row['bgimage']."' placeholder='i.imgur.com/...'><br>
        <label>Background Color: </label><input class='jscolor' name='bgcolor' type='text' value='".$row['bgcolor']."' placeholder='#XXXXXX'><br>";
    if ($row['bgattachment'] == 0) {
        echo "<span class='share_wrap'><input type='checkbox' name='bgattachment'>";
    } else {
        echo "<span class='share_wrap'><input checked type='checkbox' name='bgattachment'>";
    }
    echo " <label>Fixed Background</label></span><br>";
    if ($row['bgcover'] == 0) {
        echo "<span class='share_wrap'><input type='checkbox' name='bgcover'>";
    } else {
        echo "<span class='share_wrap'><input checked type='checkbox' name='bgcover'>";
    }
    echo " <label>Stretch Background</label></span><br>";
    echo "<span class='share_wrap'>
    <label>Background Repeat</label>
        <select name='bgrepeat'>
            <option value='0' ";
            if ($row['bgrepeat'] == 0) {
                echo 'selected';
            }
            echo ">No Repeat</option>
            <option value='1' ";
            if ($row['bgrepeat'] == 1) {
                echo 'selected';
            }
            echo ">Repeat</option>
            <option value='2' ";
            if ($row['bgrepeat'] == 2) {
                echo 'selected';
            }
            echo ">Repeat X</option>
            <option value='3' ";
            if ($row['bgrepeat'] == 3) {
                echo 'selected';
            }
            echo ">Repeat Y</option>
        </select>
    </span><br>";
    echo "<span class='share_wrap'>
    <label>Background Position</label>
        <select name='bgposition1'>
            <option value='0' ";
            if ($row['bgposition1'] == 0) {
                echo 'selected';
            }
            echo ">Top</option>
            <option value='1' ";
            if ($row['bgposition1'] == 1) {
                echo 'selected';
            }
            echo ">Center</option>
            <option value='2' ";
            if ($row['bgposition1'] == 2) {
                echo 'selected';
            }
            echo ">Bottom</option>
        </select>
        <select name='bgposition2'>
            <option value='0' ";
            if ($row['bgposition2'] == 0) {
                echo 'selected';
            }
            echo ">Left</option>
            <option value='1' ";
            if ($row['bgposition2'] == 1) {
                echo 'selected';
            }
            echo ">Center</option>
            <option value='2' ";
            if ($row['bgposition2'] == 2) {
                echo 'selected';
            }
            echo ">Right</option>
        </select>
    </span><br><br>";
    echo "<span class='share_wrap'>
    <label>Timezone:</label>
        <select name='timezone'>
            <option value='-12:00' ";
            if ($row['timezone'] == "-12:00") {
                echo "selected";
            }
            echo ">-12:00</option>
            <option value='-11:00' ";
            if ($row['timezone'] == "-11:00") {
                echo "selected";
            }
            echo ">-11:00</option>
             <option value='-10:00' ";
            if ($row['timezone'] == "-10:00") {
                echo "selected";
            }
            echo ">-10:00</option>
            <option value='-09:00' ";
            if ($row['timezone'] == "-09:00") {
                echo "selected";
            }
            echo ">-09:00</option>
            <option value='-08:00' ";
            if ($row['timezone'] == "-08:00") {
                echo "selected";
            }
            echo ">-08:00</option>
            <option value='-07:00' ";
            if ($row['timezone'] == "-07:00") {
                echo "selected";
            }
            echo ">-07:00</option>
            <option value='-06:00' ";
            if ($row['timezone'] == "-06:00") {
                echo "selected";
            }
            echo ">-06:00</option>
            <option value='-05:00' ";
            if ($row['timezone'] == "-05:00") {
                echo "selected";
            }
            echo ">-05:00</option>
            <option value='-04:00' ";
            if ($row['timezone'] == "-04:00") {
                echo "selected";
            }
            echo ">-04:00</option>
            <option value='-03:00' ";
            if ($row['timezone'] == "-03:00") {
                echo "selected";
            }
            echo ">-03:00</option>
            <option value='-02:00' ";
            if ($row['timezone'] == "-02:00") {
                echo "selected";
            }
            echo ">-02:00</option>
            <option value='-01:00' ";
            if ($row['timezone'] == "-01:00") {
                echo "selected";
            }
            echo ">-01:00</option>
            <option value='+00:00' ";
            if ($row['timezone'] == "+00:00") {
                echo "selected";
            }
            echo ">+00:00</option>
            <option value='+01:00' ";
            if ($row['timezone'] == "+01:00") {
                echo "selected";
            }
            echo ">+01:00</option>
            <option value='+02:00' ";
            if ($row['timezone'] == "+02:00") {
                echo "selected";
            }
            echo ">+02:00</option>
            <option value='+03:00' ";
            if ($row['timezone'] == "+03:00") {
                echo "selected";
            }
            echo ">+03:00</option>
            <option value='+04:00' ";
            if ($row['timezone'] == "+04:00") {
                echo "selected";
            }
            echo ">+04:00</option>
            <option value='+05:00' ";
            if ($row['timezone'] == "+05:00") {
                echo "selected";
            }
            echo ">+05:00</option>
            <option value='+06:00' ";
            if ($row['timezone'] == "+06:00") {
                echo "selected";
            }
            echo ">+06:00</option>
            <option value='+07:00' ";
            if ($row['timezone'] == "+07:00") {
                echo "selected";
            }
            echo ">+07:00</option>
            <option value='+08:00' ";
            if ($row['timezone'] == "+08:00") {
                echo "selected";
            }
            echo ">+08:00</option>
            <option value='+09:00' ";
            if ($row['timezone'] == "+09:00") {
                echo "selected";
            }
            echo ">+09:00</option>
            <option value='+10:00' ";
            if ($row['timezone'] == "+10:00") {
                echo "selected";
            }
            echo ">+10:00</option>
            <option value='+11:00' ";
            if ($row['timezone'] == "+11:00") {
                echo "selected";
            }
            echo ">+11:00</option>
            <option value='+12:00' ";
            if ($row['timezone'] == "+12:00") {
                echo "selected";
            }
            echo ">+12:00</option>
            </select>
    </span><br>
    <em>Tip: If you can't tell what your timezone is, send a screech out and it should say 'x hours ago'. Adjust the setting accordingly so it says 'x seconds/minutes ago'.</em>
    <br><br>";
	if ($globaluserinf["private"] == 0) {
        echo "<span class='share_wrap'><input type='checkbox' name='setprivate'>";
    } else {
        echo "<span class='share_wrap'><input checked type='checkbox' name='setprivate'>";
    }
    echo " <label>Protect my screeches from anyone who doesn't follow me.</label></span><br><br>
	<h2>Site settings</h2>";
    if ($globaluserinf["likes_replies"] == 0) {
        echo "<span class='share_wrap'><input type='checkbox' name='setreplies'>";
    } else {
        echo "<span class='share_wrap'><input checked type='checkbox' name='setreplies'>";
    }
    echo " <label>Show me other peoples @ replies on my homepage.</label></span><br>";
    if ($globaluserinf["likes_styles"] == 0) {
        echo "<span class='share_wrap'><input type='checkbox' name='setstyles'>";
    } else {
        echo "<span class='share_wrap'><input checked type='checkbox' name='setstyles'>";
    }
    echo " <label>Show me other people's profile styles (backgrounds, colors, etc).</label></span><br>";
    //if ($globaluserinf["likes_snow"] == 0) {
    //    echo "<span class='share_wrap'><input type='checkbox' name='setsnow'>";
    //} else {
    //    echo "<span class='share_wrap'><input checked type='checkbox' name='setsnow'>";
    //}
    //echo " <label>Show me snowflakes falling down the screen.</label></span>";
	echo "<br><center><button type=submit name='submit'>Submit</button></center>
    </form>
    <div class='settings_sep' style='width: 104%; display: block; padding: 20px 0px; background: black; margin: 2% -2%;'></div>
    <h2>Personal settings</h2>
    <span id='userpassch'>
        <span class='col'>
            <form method='post' action=''>
				<b>Change Username</b><br>
                <label>New Username:</label> <input name='newuser' type='text'><br>
                <label>Password:</label> <input name='curpassuser' type='password'><br>
                <input class='col_submit' value='Submit' name='userchange' type='submit'>
                <span class='error'>$userch_err</span>
            </form>
        </span>
        <span class='col'>
            <form method='post' action=''>
				<b>Change Password</b><br>
                <label>Old Password:</label> <input name='oldpass' type='password'><br>
                <label>New Password:</label> <input name='newpass' type='password'><br>
                <label>Confirm New Password:</label> <input name='confirmnewpass' type='password'><br>
                <input class='col_submit' value='Submit' name='passchange' type='submit'>
                <span class='error'>$passch_err</span>
            </form>
        </span>
    </span>
	<br></div>";
?>