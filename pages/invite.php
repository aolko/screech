<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.0";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<h2>Invite your friends to try out Screech!</h2>

<p>This function initally sent an email to somebody asking them to join, but <del>I'm almost certain if you're reading this in 2018 you probably have the balls to email them yourself</del> nobody really uses email anymore, so here are some other options!</p>
<br>
<span class='share_wrap'>
<a href="https://twitter.com/intent/tweet?text=Check%20out%20Screech%2C%20a%20recreation%20of%20what%20Twitter%20looked%20like%20in%202007!%20%23lol%20%23dab%20%23relatable%20%3Asunglasses%3A%20%3Asunglasses%3A%20%3Asunglasses%3A%20http%3A%2F%2Fwww.screech.xyz" class="twitter-share-button" data-size="large" data-text="" data-related="LoganPaul" data-lang="ja" data-show-count="false">Tweet (Twitter's Content Security Policy was updated so until the site gets HTTPS this won't show.)</a><br>

<div class="fb-share-button" 
  data-href="http://www.screech.xyz/" 
  data-layout="button_count">
</div>
</span>
<br><br>
We also implore you to get the word out on other sites! For example, user <a href='/KRANKO'>@KRANKO</a> set up an <em>alternateto.net</em> page! Vote for it <strong><a href="https://alternativeto.net/software/screech/">here!</a></strong>
<br><br>
<hr>
<br>
<p>As of recent, you can also now add a button to your site that allows the user to share pre-defined Screech posts.<br>
The syntax is as follows:<br>
<span class='code'><?php echo htmlspecialchars("<a target='a_blank' href='http://www.screech.xyz/post?screech=content epic style'><img alt='Screech' src='http://www.screech.xyz/resources/scr.png'></a>");?></span>
Which gives you:<br>
<a target='a_blank' href='http://www.screech.xyz/post?screech=content epic style'><img target='a_blank' alt='Screech' src='http://www.screech.xyz/resources/scr.png'></a><br>
Of course you need to replace "content epic style" with whatever you want the user to screech out (no shit sherlock). If you want something more advanced you can use the <a href='/api'>API</a>, but otherwise this is good for simplicity.