<style>
.timeago:before {content:"(";}
.timeago:after {content:")";}
</style>
<h1>Help Manual</h1>

See something that isn't on here? Want help with something that is on here? <a href="https://discord.gg/VDqwyxF"><b>Join our discord server!</b></a><br><br>

<h2 id="retweet">How do I retweet?</h2> 
<p>Retweeting is not directly supported here but you can still do it the old fashioned way: copying and pasting a tweet and adding "RT (their username)" in front of it.</p>
<br>
<h2 id="userp">I can use Screech normally but people can't visit my profile?</h2>
<p>This is likely due to two things:</p>
<ol>
    <li>You have an invalid username that wouldn't fly with a url. <em>Please contact an admin so we can be aware of the issue.</em>
    <li>Your username is also the name of a Screech page. The Screech page will always be loaded before over everything else, meaning your profile will not be loaded. <em>Please change your username via the <a href="/settings">settings page.</a></em>
</ol>
<br>
<h2 id="changepic">Why can't I change my profile picture?</h2>
<p>See <a href='/pfpupload/'>this page</a> for a solution.</p>
<br>
<b><h2 id="forgotpass">I'm trying to log in but my password isn't working...?</h2></b>
If you signed up before <b>November 21st 2018</b> <abbr old="old" class="timeago" title="2018-11-21 12:00-07:00"></abbr> you may need to contact an admin about getting your password reset. This is due to a bug in the system where passwords where being saved shorter then how you entered them, and previous passwords won't work.<br>
Type your password into <b><a href="http://www.passwordtool.hu/php5-password-hash-generator">this website</a></b> and DM the result to one of my contacts listed on the <a href='/contact'>contacts page.</a> I apologize for this inconvience.<br>
<em>If this does not apply to you:</em> there is currently no password reset method. This is planning on being implemented soon.