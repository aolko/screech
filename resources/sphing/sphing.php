<?php
////////////////////////////////////////////////////////////////////////////////////////////
//
// Sphing (Swf with PHp without mING)
//
////////////////////////////////////////////////////////////////////////////////////////////
//
// Version 0.0.1
//
////////////////////////////////////////////////////////////////////////////////////////////
//
// Currently this library is only good for jpg->swf
// Someday hopefully it will be a library for creating any kind of swf (Flash) file.
//
// Project homepage: http://sphing.sourceforge.net/
//
// Copyright 2002 Igor Clukas <igor -at- clukas -dot- de>
//
////////////////////////////////////////////////////////////////////////////////////////////
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
////////////////////////////////////////////////////////////////////////////////////////////



//////// Internal Functions

function SWFbin($bin) { // returns a byte value string from a binary string
	while (strlen($bin) % 8 != 0) $bin .= '0';
	for ($i=0; $i < strlen($bin) / 8; $i++)
		$result .= chr(bindec(substr($bin, $i*8, 8)));
	return $result;
}

function SWFdword($w) { // returns a byte value string from a dword value
	return pack('V', $w);
}

function SWFword($w) { // returns a byte value string from a word value (faster than pack?)
	return chr($w & 0xff) . chr(($w >> 8) & 0xff);
}

function SWFval2bin($v, $bits) { // returns a binary string with fixed bitlength from a numeric value
	return substr(sprintf("%0${bits}b", $v), -$bits);
}


function SWFrect($x1, $y1, $x2, $y2) { // returns a Flash rectangle structure
	$bits = (abs($x1) < 0x1fff && abs($y1) < 0x1fff &&
					 abs($y1) < 0x1fff && abs($y2) < 0x1fff) ? 14 : 16; // quick test, nothing else makes much sense
	return SWFbin(sprintf('%05b', $bits).
		SWFval2bin($x1, $bits).
		SWFval2bin($x2, $bits).
		SWFval2bin($y1, $bits).
		SWFval2bin($y2, $bits));
}

function SWFshead($id, $length) { // short tag header - for records less than 63 bytes in length
	return SWFword($id << 6 | ($length & 0x3f));
}

function SWFlhead($id, $length) { // long tag header - for records >= 63 bytes in length
	return SWFword($id << 6 | 0x3f) . SWFdword($length);
}

//////// User Functions

function SWFheader($ver, $fsize, $w, $h, $rate, $frames) {
	$result = SWFrect(0,0,$w*20,$h*20);
	if ($rate != (int)$rate) {
		$ratefrac = ($rate - (int)$rate) * 256;
		$rate = (int)$rate;
	}
	$result .= chr($ratefrac) . chr($rate);
	$result .= SWFword($frames);
	$fsize += strlen($result) + 8;
  return 'FWS' . chr($ver) . SWFdword($fsize) . $result;
}

function SWFsetbgcolor($r, $g, $b) {
	return SWFshead(9, 3) . chr($r) . chr($g) . chr($b); 
}

function SWFdefinejpeg2($filename, $id) {
	$result = SWFlhead(21, filesize($filename)+6);
	$result .= SWFword($id); // bitmap id
  $result .= SWFword(0xd9ff);
  $result .= SWFword(0xd8ff);
  $fp = @fopen($filename, 'rb');
  set_magic_quotes_runtime(0);
  $result .= fread($fp, filesize ($filename));
  set_magic_quotes_runtime(get_magic_quotes_gpc());
  fclose($fp);
	return $result;
}

function SWFdefineshape($x1, $y1, $x2, $y2, $fills, $fillstyles, $lines, $linestyles, $shapes, $id) { // Shape1 only!
	$result = SWFword($id);
	$result .= SWFrect($x1, $y1, $x2, $y2); // bounds

	if ($fills != 0) {
		for ($fbits=1; $fbits <= 8; $fbits++)
			if ($fills >> $fbits == 0) break;
	} else
		$fbits = 0;
	if ($lines != 0) {
		for ($lbits=1; $lbits < 8; $lbits++)
			if ($lines >> $lbits == 0) break;
	} else
		$lbits = 0;
	$result .= chr($fills) . $fillstyles; // # of fill styles and fill styles array
	$result .= chr($lines) . $linestyles; // # of line styles and line styles array
	$result .= chr(($fbits << 4) | $lbits); // fill and line style index bits
	$result .= $shapes;

	return SWFlhead(2, strlen($result)) . $result;
}

function SWFfillstylebitmap($id) { // this is totally INCOMPLETE and currently only a hack!
	return chr(0x41).SWFword($id).
		SWFbin('1'.	// has scale
					 '10110'.	// 22 bits (4.16)
					 '0101000000000000000000'.	// 20
					 '0101000000000000000000'.	// 20
					 '0'.	// has rotate
					 '0'	// translate bits
					 );
}

function SWFshapebox($x1, $y1, $x2, $y2, $fill) { // creates a box shape, INCOMPLETE
	$max = abs($x1);
	if (abs($x2) > $max) $max = abs($x2);
	if (abs($y1) > $max) $max = abs($y1);
	if (abs($y2) > $max) $max = abs($y2);
	if ($max != 0) {
		for ($bits=1; $bits < 32; $bits++)
			if ($max >> $bits == 0) break;
		$bits++; // extra bit for signed values...
		if ($bits < 2) $bits = 2;
	} else
		$bits = 2; // minimum of 2 because:
	$bitsl = $bits-2; // in line records Flash adds 2 to the bitcount
	
	return SWFbin(
		'0'.	// non-edge record flag
		'0'.	// new styles flag
		'0'.	// line style change flag
		'1'.	// fill style 1 change flag
		'0'.	// fill style 0 change flag
		'1'.	// move to flag
		sprintf('%05b',$bits).////14
		SWFval2bin($x2, $bits).	// move to x2,y2
		SWFval2bin($y2, $bits).
		sprintf('%01b', $fill).	// fill style (bit length should be the same as in fill style array!)
		'11'. // edge record
		sprintf('%04b',$bitsl).
		'00'. // gen line = no, vert line = no
		SWFval2bin($x1-$x2, $bits).
		'11'. // edge record
		sprintf('%04b',$bitsl).
		'01'. // gen line = no, vert line = yes
		SWFval2bin($y1-$y2, $bits).
		'11'. // edge record
		sprintf('%04b',$bitsl).
		'00'. // gen line = no, vert line = no
		SWFval2bin($x2-$x1, $bits).
		'11'. // edge record
		sprintf('%04b',$bitsl).
		'01'. // gen line = no, vert line = yes
		SWFval2bin($y2-$y1, $bits).
		'000000');	// end of shape record
}

function SWFplaceobject($id, $depth) { // placeobject tag, INCOMPLETE
	$result = SWFshead(26,6);
	$result .= chr(0x06);	// has matrix, has character
	$result .= SWFword($depth);
	$result .= SWFword($id);
	$result .= chr(0);	// empty matrix
	return $result;
}

function SWFshowframe() { // display current frame
	return SWFshead(1,0);
}

function SWFendtag() { // bye-bye!
	return SWFshead(0,0);
}

///////// Example Functions

function jpg2swf($jpgfile, $width, $height) {
	$result = SWFsetbgcolor(0x00,0x00,0x00);
	$result .= SWFdefinejpeg2($jpgfile, 1);
	$result .= SWFdefineshape(0,0,$width*20,$height*20,
		1,SWFfillstylebitmap(1),0,'',SWFshapebox(0,0,$width*20,$height*20,1),2);
	$result .= SWFplaceobject(2,1);
	$result .= SWFshowframe();
	$result .= SWFendtag();
	return SWFheader(3, strlen($result), $width, $height, 12, 1) . $result;
}

?>