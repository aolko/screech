<?php
include('resources/connect.php');
$tweetid = str_replace("statuses","",$link);
$tweet_query_single = "SELECT * FROM tweets WHERE id = '$tweetid' ORDER BY CAST(id as SIGNED INTEGER) DESC LIMIT 9";
$tweet_single_result = mysqli_query($conn, $tweet_query_single); 
$tweetrow_single = mysqli_fetch_assoc($tweet_single_result);
$userinf = mysqli_fetch_assoc(mysqli_query($conn, "SELECT * from `users` WHERE username = '".$tweetrow_single['username']."'"));
$prof_user = $userinf['username'];
$prffresult = mysqli_query($conn, "SELECT * FROM following WHERE user2 = '".$tweetrow_single['username']."'");
$prffollowers=array();
while($prffrow = mysqli_fetch_assoc($prffresult)) {
    foreach ($prffrow as $key=>$value) {
        if (strpos($value, $prffrow['user1']) === false) {
            continue;
        }
        if(!in_array($value, $prffollowers)){
            $prffollowers[]=$prffrow['user1'];
        }
    }
}
$prffollowers[]=$tweetrow_single['username'];
$isFriends = 1;
if($userinf['private'] == 0) {
    $isFriends = 1;
} else {
    if(in_array($log_name, $prffollowers)) {
		$isFriends = 1;
	} else {
		$isFriends = 0;
	}
}
if($userinf['private'] == 1) {
    echo("");
} else {
    echo "
    <meta property='og:type' content='website'>
<meta property='og:title' content='".$tweetrow_single['username']." on Screech'>
<meta property='og:description' content='\"".$tweetrow_single['tweet']."\"' />
<meta property='og:url' content='http://www.screech.xyz/statuses/".$tweetrow_single['id']."'/>
<meta property='og:image' content='http://www.screech.xyz/profiles/images/".$tweetrow_single['username'].".png'>
<meta property='og:image:width' content='256'>
<meta property='og:image:height' content='256'>
<title>".$tweetrow_single['username']." on Screech: ".$tweetrow_single['tweet']."</title>";
}
if($isFriends == 0) {
    die("<h1>This account is private. You cannot see this tweet unless you are following them.</h1>");
}
$aquery = mysqli_query($conn, "SELECT * FROM `apps` WHERE `appname` = '".$tweetrow_single["sentfrom"]."'");
$aresult = mysqli_fetch_assoc($aquery);
if(mysqli_num_rows($aquery) == 1) {
    $sentfrom = "<a href='".$aresult['applink']."'>".$tweetrow_single["sentfrom"]."</a>";
} else {
    $sentfrom = $tweetrow_single["sentfrom"];
}
if (isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] == true) {
$favorite = "<span id='".$tweetrow_single["id"]."' class='tweetbutton favorite'>[Favorite]</span>";
    if(in_array($tweetrow_single["id"], $personalfavourings)){
        $favorite = "<span id='".$tweetrow_single["id"]."' class='tweetbutton favorited'>[Unfavorite]</span>";
    }
} else {
    $favorite = "";
}
echo "<span class='tweet_single'>
          <span class='tweet_single_content'>".$tweetrow_single['tweet']."</span>
          <span class='tweet_single_timestamp'>$favorite <span class='timeago' title='".$tweetrow_single["timestamp"]."".$globaluserinf['timestamp']."'>".$tweetrow_single["timestamp"]."</span> from $sentfrom</span>
      </span>";
if (empty($tweetrow_single)) {
    header("Location: /$tweetid");
}
?>