<?php 
error_reporting(E_ALL);
session_start();
include($_SERVER["DOCUMENT_ROOT"].'/config.php');
include($_SERVER["DOCUMENT_ROOT"].'/resources/connect.php');
$prof_user = $_SESSION["username"];
$log_name = $prof_user;
$fullName = htmlspecialchars($_POST['fullname']);
$bio = $_POST['bio'];
$location = htmlspecialchars($_POST['location']);
$website = htmlspecialchars($_POST['website']);
$bgattachment = $bgcover = 0;
$bgrepeat = $_POST['bgrepeat'];
$bgposition1 = $_POST['bgposition1'];
$bgposition2 = $_POST['bgposition2'];
$sibgcolor = $_POST['sibgcolor'];
if(!isset($sibgcolor) || $sibgcolor == null) {
	$sibgcolor = "ffffff";
}
$sibdcolor = $_POST['sibdcolor'];
$timezone = $_POST['timezone'];
if (isset($_POST['sitcolor'])) {
    $sitcolor = " sitcolor = \"".$_POST['sitcolor']."\",";
} else {
    $sitcolor = "";
}
if (isset($_POST['silcolor'])) {
    $silcolor = " silcolor = \"".$_POST['silcolor']."\",";
} else {
    $silcolor = "";
}
if ($_POST['bgcolor'] !== "FFFFFF") {
    $bgcolor = $_POST['bgcolor'];
} else {$bgcolor = "";}
if (isset($_POST['bgimage'])) {
    $bgimage = $_POST['bgimage'];
} else {$bgimage = "";}
if (isset($_POST['setreplies'])) {
    $replyup = '1';
    $_SESSION['likes_replies'] = 1;
} else {
    $replyup = '0';
    $_SESSION['likes_replies'] = 0;
}
if (isset($_POST['setsnow'])) {
    $snowup = '1';
} else {
    $snowup = '0';
}
if (isset($_POST['setprivate'])) {
    $privup = '1';
} else {
    $privup = '0';
}
if (isset($_POST['setstyles'])) {
    $styleup = '1';
    $_SESSION['likes_styles'] = 1;
} else {
    $styleup = '0';
    $_SESSION['likes_styles'] = 0;
}
if (isset($_POST['bgattachment'])) {
    $bgattachment = 1;
} else {
    $bgattachment = 0;
}
if (isset($_POST['bgcover'])) {
    $bgcover = 1;
} else {
    $bgcover = 0;
}
$bio = str_replace("\"","'",$bio);
$bio = str_replace("<?","",$bio);
$bio = str_replace("?>","",$bio);
$bio = str_replace("<script>","",$bio);
$bio = str_replace("</script>","",$bio);
$website = str_replace("javascript:","xxxxxxxxx:",$website);
$profsql = "UPDATE users SET fullname=?, bio=?, location=?, webpage=?, private=?, likes_replies=?, likes_styles=?, likes_snow=?,  sibgcolor=?, sibdcolor=?, silcolor=?, sitcolor=?, bgcolor=?, bgimage=?, bgattachment=?, bgcover=?, bgrepeat=?, bgposition1=?, bgposition2=? WHERE username=?";
$target_dir = $_SERVER["DOCUMENT_ROOT"]."/uploads/";
$target_file = $_SERVER["DOCUMENT_ROOT"]."/profiles/images/$log_name.png";
$uploadOk = 1;
$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
if (is_uploaded_file($_FILES["fileToUpload"]["tmp_name"])) {
    $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
    if($check !== false) {
        echo "";
    } else {
        echo "File is not an image.";
        die;
    }
    if ($uploadOk == 0) {
        echo "Sorry, your file was not uploaded.<br><b>This is a known bug with Screech. We're getting a new server soon, but for now try a different picture or ask an admin to upload the picture for you.</b>";
    } else {
        if(move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
            echo "";
        } else {
            die("The profile pic didn't upload. Try again or try downsizing it.");
        }
    }
}

if($stmt = mysqli_prepare($conn, $profsql)) {
	mysqli_stmt_bind_param($stmt, "ssssiiiissssssiiiiis", $fullName, $bio, $location, $website, $privup, $replyup, $styleup, $snowup, $sibgcolor, $sibdcolor, $silcolor, $sitcolor, $bgcolor, $bgimage, $bgattachment, $bgcover, $bgrepeat, $bgposition1, $bgposition2, $log_name);
	if (mysqli_stmt_execute($stmt)) {
		mysqli_set_charset($conn, 'utf8');
		header("Location: /settings");
	} else {
		echo "Error updating record: " . mysqli_error($conn);
	}
} else {
	echo "Failed to prepare SQL: ".(string)$stmt;
}
?>
