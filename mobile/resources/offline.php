<script>
    username = window.localStorage.getItem("username");
    password = window.localStorage.getItem("password");
    if(username != null) {
        $.get("/login?username="+username+"&password="+encodeURI(password)+"");
        var d = new Date();
        d.setTime(d.getTime() + (30*24*60*60*1000));
        document.cookie = "username="+username+"; expires="+d.toUTCString()+"; path=/"; 
    } else {
        window.location.href = "/login";
    }
</script>