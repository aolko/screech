<?php
    $search_query = $_GET['q'];
    if(empty($_GET['q'])) {
        echo "
                 <form action='search'><p>
                    <input type='text' name='q' style='width: 100%'>
                    <input type='submit' value='Search'>
                </p></form>
                ";
    } else {
    $search_arr = explode(' ',$search_query);
    array_walk($search_arr, function(&$x) {$x = "'$x'";});
    $usersearch_sql = "SELECT * FROM users WHERE (username LIKE '%$search_query%' OR fullname LIKE '%$search_query%' OR username IN (".implode(',', $search_arr).") OR fullname IN (".implode(',', $search_arr).")) AND (username NOT IN ('" . implode("','", $privateusers) . "') AND username NOT IN ('" . implode("','", $bannedusers) . "'))";
    $tweetsearch_sql = "SELECT * FROM tweets WHERE tweet LIKE '%$search_query%' AND (username NOT IN ('" . implode("','", $privateusers) . "') AND username NOT IN ('" . implode("','", $bannedusers) . "'))";
    $usersearch_result = mysqli_query($conn, $usersearch_sql);
    $usersearch_no = mysqli_num_rows($usersearch_result);
    $tweetsearch_result = mysqli_query($conn, $tweetsearch_sql);
    $tweetsearch_no = mysqli_num_rows($tweetsearch_result);
    $oddeven = 0;
    $content = "";
    $alreadyfound = array();
    if ($usersearch_no >= 1) {
        echo "<div class='search_users'><b>Users</b><br>";
    }
    while($usersearch_row = mysqli_fetch_assoc($usersearch_result)) {  
        foreach ($usersearch_row as $key=>$value) {
            if (strpos($value, $usersearch_row["username"]) === false) {
                continue;
            }
            if (in_array($usersearch_row['username'], $alreadyfound)) {
                continue;
            }
            echo "
                <span class='search_user_result'>
                    <a title=".$usersearch_row['username']." href='/".$usersearch_row['username']."'>
                    <img width='50' height='50' src='/profiles/images/".$usersearch_row['username'].".png'></a><br>
                    <strong><a href='".$usersearch_row['username']."'>".$usersearch_row['username']."</a></strong><br>
                    <em>".$usersearch_row['fullname']."</em>
                </span>";
            $alreadyfound[]=$usersearch_row['username'];
        }
    }
    if ($usersearch_no >= 1) {
        echo "</div>";
    }
    if ($tweetsearch_no >= 1) {
        echo "<div class='search_tweets'><b>Screeches</b><br>";
    }
    while($tweetsearch_row = mysqli_fetch_assoc($tweetsearch_result)) {  
        foreach ($tweetsearch_row as $key=>$value) {
            if (strpos($value, $tweetsearch_row["tweet"]) === false) {
                continue;
            }
            if (in_array($tweetsearch_row['tweet'], $alreadyfound)) {
                continue;
            }
            $favorite = "<span id='".$tweetsearch_row["id"]."' class='tweetbutton favorite'>[Favorite]</span>";
            if ($oddeven == 0) {
                $oddevenval = "odd";
                $oddeven = 1;
            } else {
                $oddevenval = "even";
                $oddeven = 0;
            }
            $content = str_replace($search_query, '<b>'.$search_query.'</b>', $tweetsearch_row["tweet"]);
            echo("
                <div class='$oddevenval publictimeline_tweet'>
                    <span class='user_actions'>
                    </span>
                    <span class='publictimeline_prof_wrapper'>
                        <img width='48px' height='48px' src='/profiles/images/".$tweetsearch_row["username"].".png'>
                    </span>
                    <span class='publictimeline_tweet_content'>
                        <b><a href='".$tweetsearch_row["username"]."'>".$tweetsearch_row["username"]."</a></b> <span class='tweetconwrap'>$content</span>
                        <span class='timestamp'><a href='/statuses/".$tweetsearch_row["id"]."''><span class='timeago' title='".$tweetsearch_row["timestamp"]."'>".$tweetsearch_row["timestamp"]."</span></a> from ".$tweetsearch_row["sentfrom"]."</span>
                </div>");
            $alreadyfound[]=$tweetsearch_row['tweet'];
        }
    }
    if ($tweetsearch_no >= 1) {
        echo "</div>";
    }
    }
?>