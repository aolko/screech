<?php
    include('connect.php');
    error_reporting(E_ALL); ini_set('display_errors', 1); 
	$perms = 0;
    $appname = $_POST['appname']; $appimg = $_POST['appimg']; $applink = $_POST['applink'];$appowner=$_POST['appowner'];$appdesc=$_POST['appdesc'];$log_name=$_POST['log_name'];$action=$_POST['action'];
    $app_err = "";
    if($action == 1) {
        if($_POST['appowner'] != $_POST['log_name']) {
            $app_err = "You are not the owner of that app.";
        }
        if(strpos($_POST['appname'], ' ') !== false) {
            $app_err = "App cannot contain any spaces.";
        }
        $ncheckq = mysqli_query($conn, "SELECT * FROM `apps` WHERE appname = '".$_POST['appname']."'");
        $ncheck = mysqli_fetch_assoc($ncheckq);
        if(mysqli_num_rows($ncheckq) >= 1) {
            if($ncheck['client_id'] != $_POST['appid']) {
                $app_err = "An app with that name already exists.";
            } else {
                echo "";
            }
        }
        if (empty($app_err)) {
            if(mysqli_query($conn, "UPDATE `apps` SET appname='$appname',appdesc='$appdesc',appimg='$appimg',applink='$applink' WHERE client_id='".$_POST['appid']."' AND owner='$appowner'")) {
                if(mysqli_query($conn, "UPDATE `auth` SET appname='$appname' WHERE appname='".$_POST['appn']."'")) {
                    echo "Information successfully updated for app \"".$_POST['appname']."\"";
                } else {
                    echo mysqli_error($conn);
                }
            } else {
                echo mysqli_error($conn);
            }
        }
    }
    if($action == 2) {
        if($_POST['appowner'] != $_POST['log_name']) {
            $app_err = "You are not the owner of that app.";
        }
        if($_POST['delconfirm'] != $_POST['appname']) {
            $app_err = "Wrong app name";
        }
        if (empty($app_err)) {
            if(mysqli_query($conn, "DELETE FROM `apps` WHERE appname = '$appname' AND owner='$appowner'")) {
                $app_err = "Information successfully updated for app \"".$_POST['appname']."\"";
            } else {
                echo mysqli_error($conn);
            }
        }
    }
    if($action == 3) {
        function getRandomString($length = 8, $type = 1) {
            tryagain:
            if ($type == 1) {
                $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            } elseif ($type == 2) {
                $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            }
            $string = '';
            for ($i = 0; $i < $length; $i++) {
                $string .= $characters[mt_rand(0, strlen($characters) - 1)];
            }
            if($checkun = mysqli_query($GLOBALS['conn'], "SELECT * FROM `apps` WHERE client_secret = $string OR client_id = $string")) {
                goto tryagain;
            }
            return $string;
        }
        if($_POST['appowner'] != $_POST['log_name']) {
            $app_err = "You are not the owner of that app.";
        }
        if (empty($app_create_err)) {
            if(mysqli_query($conn, "UPDATE `apps` SET client_id='".getRandomString(24, 1)."',client_secret='".getRandomString(16, 2)."' WHERE appname='$appname' AND owner='$appowner'")) {
                $app_err = "Information successfully updated for app \"".$_POST['appname']."\"";
            } else {
                echo mysqli_error($conn);
            }
        }
    }
    echo $app_err."    <script>
                function sleep(ms) {
                    return new Promise(resolve => setTimeout(resolve, ms));
                }
                async function what() {
                    await sleep(10);
                    window.location.reload();
                }
                what();</script>";
 ?>