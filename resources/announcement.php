<script>
	function dismiss_announcement() {
		$.get("/ajax/dismiss_announcement.php", {}, function(r) {
			var result = JSON.parse(r);
			if(result.status == "success") {
				$("#announcement").slideUp();
			} else {
				console.log("Unknown error occurred: "+r);
			}
		});
	}
</script>
<?php if($config["announcement"] != "" && !isset($_SESSION["dismiss_announcement"])): ?>
	<div id="announcement">
		<center>
			<?=$config["announcement"]?>
		</center>
		<span style="position:relative;top:-23px;right:5px;text-align:right;float:right" onclick="dismiss_announcement	()">
			x
		</span>
	</div>
<?php endif; ?>
