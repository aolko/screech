<?php
$test = $_GET['test'];
if ($test == 1) {
    error_reporting(E_ALL); ini_set('display_errors', 1); 
}
    //?post=<what you're doing>&select=<what its affecting>&user=<who its affecting>&content=<content>&client=<client id>&secret=<client secret>&id=<id>
    $post = $_GET['post']; $select = $_GET['select']; $user = $_GET['user']; $client = $_GET['client']; $secret = $_GET['secret']; $content=$_GET['content']; $image=file_get_contents($content);
    $id = $_GET['id'];
    $apperror = "";
    include('/var/www/screech/resources/connect.php');
    $cquery = mysqli_query($conn, "SELECT * FROM `apps` WHERE client_id = '$client' AND client_secret = '$secret'");
    $cresult = mysqli_fetch_assoc($cquery);
    $aquery = mysqli_query($conn, "SELECT * FROM `users` WHERE username = '$user'");
    $aresult = mysqli_fetch_assoc($aquery);
    if($aresult['banned'] == 1) {
        echo "This user has been banned.";
        die();
    }
    if(mysqli_num_rows($cquery) <= 0) {
        echo "Invalid or non-existant app (make sure the client ID and client secret are correct)";
        die();
    }
    if($cresult['permissions'] <= 1) {
        echo "Invalid permissions.";
        die();
    }
    $dupcheck = mysqli_query($conn, "SELECT * FROM tweets WHERE tweet = '$content' AND username = '$user' AND timestamp BETWEEN DATE_SUB(NOW(), INTERVAL 30 MINUTE) AND NOW();");
    if(mysqli_num_rows($dupcheck) >= 1) {
        $tweeterr = "You screeched this same thing less then 30 minutes ago. Please try again.";
    }
    $spamcheck = mysqli_query($conn, "SELECT * FROM tweets WHERE username = '$user' AND timestamp BETWEEN DATE_SUB(NOW(), INTERVAL 30 SECOND) AND NOW();");
    if(mysqli_num_rows($spamcheck) >= 1) {
        $tweeterr = "You screeched something less then 30 seconds ago. Please wait before trying again.";
    }
    $authquery = mysqli_query($conn, "SELECT * FROM `auth` WHERE appname = '".$cresult['appname']."' AND username = '$user'");
    $authresult = mysqli_fetch_assoc($authquery);
    if(mysqli_num_rows($authquery) <= 0) {
        echo "This user has not autherized themselves to use your app.";
        die();
    }
    if($post == "post") {
        $content = str_replace("\"", "\\\"", $content);
         if (empty($content)) {
             echo "Cannot post an empty screech.";
             die();
         }
         if ($content > 140) {
             echo "Screech is too long.";
             
         }
         if (empty($user)) {
             echo "Cannot post as an empty user";
         }
         if($stmt = mysqli_prepare($conn, "INSERT INTO tweets (username, tweet, sentfrom) VALUES (?, ?, ?)")){
             mysqli_stmt_bind_param($stmt, "sss", $user, mysqli_real_escape_string($conn, htmlspecialchars($content)), $cresult['appname']);
             if(mysqli_stmt_execute($stmt)){
                 echo "Successfully posted!";
             } else{
                 echo "Error: (".mysqli_error($conn)."). For a more detailed error use ?test=1";
             }
         }
     }
     if($post == "profile") {
         if($select == "bio" ||$select == "location" ||$select == "private" || $select == "webpage" || $select == "bgcolor" || $select == "sibgcolor" || $select == "sibdcolor" || $select == "sitcolor" || $select == "silcolor" || $select == "bgimage" || $select == "bgattachment" || $select == "bgcover" || $select == "bgrepeat" || $select == "bgposition1" || $select == "bgposition2" || $select == "likes_replies" || $select == "likes_styles" || $select == "likes_snow") {
            if(mysqli_query($conn, "UPDATE `users` SET $select='$content' WHERE username = '$user'")) {
                echo "Successfully updated $user's $select!";
            } else {
                echo "Error: (".mysqli_error($conn)."). For a more detailed error use ?test=1";
            }
         }
     }
     if($post == "favorite") {
         if(mysqli_query($conn, "INSERT INTO favorites (user, tweetid) VALUES ('$user', '$id')")) {
             echo "Screech successfully favorited!";
         } else {
             echo "Error: (".mysqli_error($conn)."). For a more detailed error use ?test=1";
         }
     }
     if($post == "unfavorite") {
         if(mysqli_query($conn, "DELETE FROM favorites WHERE user='$user' AND tweetid='id'")) {
             echo "Screech successfully favorited!";
         } else {
             echo "Error: (".mysqli_error($conn)."). For a more detailed error use ?test=1";
         }
     }
     if($post == "follow") {
         if(empty($select)) {
             echo "No user to follow.";
             die();
         }
         $userinfq = mysqli_query($conn, "SELECT * FROM users WHERE username = '$select'");
         $userinf = mysqli_fetch_assoc($userinfq);
         if($userinf["private"] == 1) {
             if(mysqli_query($conn, "INSERT INTO requests (user1, user2) VALUES ('$user', '$select')")) {
                 echo "Request successfully sent (user is private)";
             } else {
                 echo "Error: (".mysqli_error($conn)."). For a more detailed error use ?test=1";
             }
         } else {
             if(mysqli_query($conn, "INSERT INTO following (user1, user2) VALUES ('$user', '$select')")) {
                 echo "User successfully followed.";
             } else {
                 echo "Error: (".mysqli_error($conn)."). For a more detailed error use ?test=1";
             }
         }
     }
     if($post == "unfollow") {
         if(empty($select)) {
             echo "No user to follow.";
             die();
         }
         if(mysqli_query($conn, "DELETE from following WHERE user1='$user' AND user2='$select'")) {
             echo "User successfully unfollowed.";
         } else {
             echo "Error: (".mysqli_error($conn)."). For a more detailed error use ?test=1";
         }
     }
    if(isset($image) && !empty($image) && $post == "avatar") {
        if(empty($image)) {
            echo "Image must be a link to a pastebin or another online file containing the base 64 for the image.";
        }
        list($type, $image) = explode(';', $image);
        list(, $image)      = explode(',', $image);
        $image = base64_decode($image);
        file_put_contents("/var/www/screech/profiles/images/$user.png", $image);
        echo "File uploaded!";
    }
?>