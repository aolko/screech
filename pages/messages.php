<?php
    if(empty($log_name)) {
        header("Location: /login");
    }
    $message_error = "";
    if($_SERVER["REQUEST_METHOD"] == "POST") {
        if (isset($_POST['message_send'])) {
            error_reporting(E_ALL); ini_set('display_errors', 1); 
            $userExists = 0;
            $recipient = $_POST['recipient'];
            $subject = htmlspecialchars($_POST['subject']);
            $message = htmlspecialchars($_POST['message']);
            $message = str_replace("\'", "\\'", $message);
            $subject = str_replace("\'", "\\'", $subject);
            $usersql = "SELECT * from users WHERE username = '$recipient'";
            $usercheck = mysqli_query($conn, $usersql);
            while($userrow = mysqli_fetch_assoc($usercheck)) {
                foreach ($userrow as $key=>$value) {
                    $userExists = 1;
                }
            }
            if($userExists == 0) {
                $message_error = "That user does not exist.";
            }
            $sendsql = "INSERT INTO messages (user1, user2, subject, message) VALUES (?, ?, ?, ?)";
            if (empty($message_error)) {
                if($stmt = mysqli_prepare($conn, $sendsql)){
                    mysqli_stmt_bind_param($stmt, "ssss", $log_name, $recipient, mysqli_real_escape_string($conn, $subject), mysqli_real_escape_string($conn, $message));
                    if(mysqli_stmt_execute($stmt)){
                        header("Location: /messages");
                    } else{
                        echo "Something went wrong. Please try again later.";
                    }
                }
            }
        }
        echo "<script>
        $('#message_send')[0].reset();
        </script>";
    }
?>
<h2>Messages</h2>
<table class='pm_table' cellpadding="10" border="1">
        <thead>
            <td>Send Messages</td>
            <td>Your Messages</td>
        </thead>
        <tbody>
        <tr>
        <td>
            <form id='message_send' method='post' action=''>
            <label>User:</label> <input id='pm_recipient' name='recipient' type='text' value="<?php echo $recipient;?>"<br>
            <label>Subject:</label> <input id='pm_subject' name='subject' type='text' value="<?php echo $subject;?>"><br>
            <label>Message:</label><br><textarea name='message'><?php echo $message;?></textarea><br>
            <input type='submit' name='message_send' value='Send'><br>
            </form>
            <span class='error'><?php echo $message_error;?></span>
        </td>
        <td>
            <?php
                $oddeven = 0;
                $oddevenval = "odd";
                $messagegetsql = "SELECT * from messages WHERE user2 = '$log_name' OR user1 = '$log_name' ORDER BY CAST(id as SIGNED INTEGER) DESC";
                $messageget = mysqli_query($conn, $messagegetsql);
                while($messagegetrow = mysqli_fetch_assoc($messageget)) {
                    foreach ($messagegetrow as $key=>$value) {
                        if (strpos($value, $messagegetrow["message"]) === false) {
                            continue;
                        }
                        if (strpos($value, $messagegetrow["message"]) === false) {
                            continue;
                        }
                            if ($oddeven == 0) {
                                $oddevenval = "odd";
                                $oddeven = 1;
                            } else {
                                $oddevenval = "even";
                                $oddeven = 0;
                            }
                        $subjectget = "<strong>".htmlspecialchars($messagegetrow["subject"])."</strong>";
                        if ($subjectget == "<strong></strong>") {
                            $subjectget = "<em>No subject.</em>";
                        }
                        if($messagegetrow["user1"] == $log_name) {
                            $oddevenval = "you";
                        }
                        echo "<span class='pm_item $oddevenval'>
                        <span class='pm_content'>";
                        if($messagegetrow["user1"] == $log_name) {
                            echo "You -> <a href='/".$messagegetrow["user2"]."'>".$messagegetrow["user2"]."</a>:";
                        } else {
                            echo "<a href='/".$messagegetrow["user1"]."'>".$messagegetrow["user1"]."</a>: ";
                        }
                        echo "
                        <span onclick=\"$(\'.pm_message\').removeClass(\'show\'); $(\'.pm_message\').addClass(\'hide\'); $(\'.pm_message_".$messagegetrow["id"]."\').toggleClass(\'hide\', \'show\');\"
                        class='pm_subject'>
                        $subjectget
                        </span>
                        <em>(sent <span class='pm_time timeago' title='".$messagegetrow["time"]."".$globaluserinf['timestamp']."'>".$messagegetrow["time"].")</span>)</em>
                        <br><span class='hide pm_message pm_message_".$messagegetrow["id"]."'>
                        \"".htmlspecialchars($messagegetrow["message"])."\"
                        </span>
                        </span>
                        <span onclick=\"pmrepl(\'".$messagegetrow["user1"]."\', \'".$messagegetrow["subject"]."\')\" class='pm_reply'>
                        Reply
                        </span>
                        </span>";
                        mysqli_query($conn, "UPDATE messages SET has_read = '1' WHERE id = ".$messagegetrow["id"]."");
                    }
                }
            ?>
        </td>
        </tbody>
    </tr>
</table>
<strong>NOTE: To see the message contents, click on the Subject area next to the username.</strong>