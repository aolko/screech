
<p>
Let your Myspace, FriendProject, or Neocities friends know why you’re not online with customizable Screech badges!
</p>
<h2>iFrame:</h2>
<label>Widget Width: <span class='width_pre'>200</span> </label> <input id='iframe_width' type='range' value='200' min='0' max='2000'><br>
<label>Widget Height: <span class='height_pre'>300</span></label><input id='iframe_height' type='range' value='300' min='0' max='2000'><br>
<label>Number of Screeches shown: </label> <input id='iframe_sno' value='5' type='text'><br>
<span class='code'>
    <?php echo htmlspecialchars("<iframe ");
        echo "style='border: 1px solid black' frameborder='0' width=\"<span class='width_pre'>200</span>\" height=\"<span class='height_pre\'>300</span>\"
              src='www.screech.xyz/badge?p=$log_name&limit=<span class='limit_pre'>5</span>'>";
        echo htmlspecialchars("</iframe>");?>
</span><br>
<iframe id='iframe_pre' style='border: 1px solid black' frameborder='0' width='200' height='300' src='/badge?p=<?php echo $log_name;?>'></iframe>
<br>
<h2>Flash:</h2>
<span class='code'>
    <?php echo htmlspecialchars("<object ");
        echo "data='http://www.screech.xyz/resources/ScreechBadge2007.swf'
        style='border: 1px solid black' width='300' height='400' quality='heigh'
              ";
              echo htmlspecialchars("><param name=FlashVars value=\"");
               echo "user=$log_name'>";
        echo htmlspecialchars("</object>");?>
</span><br>
<object id='flash_pre' data='/resources/ScreechBadge2007.swf' style='border: 1px solid black;' width='300' height='400'>
<param name="quality" value="high">
<param name=FlashVars value="user=<?php echo $log_name;?>&limit=5">
</object>