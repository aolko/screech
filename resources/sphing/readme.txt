Sphing 0.0.1 release [03/06/02]
===============================

What's in the package?
sphing.php    - the library itself
swftest.html  - example 1
swftest.php   - used in example 1
swftest2.php  - example 2
swftest3.html - example 3
swftest3.php  - used in example 3
pic.jpg       - used in examples 1,2
pic2.jpg      - used in example 3

Project homepage: http://sphing.sourceforge.net/

