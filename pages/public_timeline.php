<div class='public_timeline'>
<?php
    if($theme == 2008) {
        echo "<h2 style='margin: 13px 0 4px 0; font-weight: bold'>Recent Public Updates</h2>";
    }
    $oddeven = 0;
    $contenttotweet = "";
    $get_all = $_GET['get_all'];
    if (empty($get_all)) {
        $get_all = 0;
    }
    if(empty($limit)) {
        $limit = 100;
    }
    if(!$limit == 9) {
        echo "<h2>Last $limit Public Updates</h2>";
    }
    if($get_all = 0) {
        $tweetlinesql = "SELECT * FROM `tweets` WHERE tweet (NOT RLIKE '@([^\s]+){1,}' AND NOT LIKE 'RT' AND (username NOT IN ('" . implode("','", $privateusers) . "') AND username NOT IN ('" . implode("','", $bannedusers) . "')) ORDER BY CAST(id as SIGNED INTEGER) DESC LIMIT $limit";
    } else {
        $tweetlinesql = "SELECT * FROM `tweets` WHERE (username NOT IN ('" . implode("','", $privateusers) . "') AND username NOT IN ('" . implode("','", $bannedusers) . "')) ORDER BY CAST(id as SIGNED INTEGER) DESC LIMIT $limit";
    }
    $tweetlineresult = mysqli_query($conn, $tweetlinesql);
    if(!$tweetlineresult) {
        echo mysqli_error($conn);
    }
    while($tweetlinerow = mysqli_fetch_assoc($tweetlineresult)) {
        foreach ($tweetlinerow as $key=>$value) {
            if (strpos($value, $tweetlinerow["tweet"]) === false) {
                continue;
            }
            if ($oddeven == 0) {
                $oddevenval = "odd";
                $oddeven = 1;
            } else {
                $oddevenval = "even";
                $oddeven = 0;
            }
            if (!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true) {
            $favorite = $usercontent = "";
            } else {
                $favorite = "<span id='".$tweetlinerow["id"]."' class='tweetbutton favorite'>[Favorite]</span>";
                if(in_array($tweetlinerow["id"], $personalfavourings)){
                    $favorite = "<span id='".$tweetlinerow["id"]."' class='tweetbutton favorited'>[Unfavorite]</span>";
                }
                if($tweetlinerow["username"] == $log_name) {
                    $usercontent = "<span id='".$tweetlinerow["id"]."' class='tweetbutton delete'>[Delete]</span>";
                } else {
                    $usercontent = "";
                }
            }
            $aquery = mysqli_query($conn, "SELECT * FROM `apps` WHERE `appname` = '".$tweetlinerow["sentfrom"]."'");
            $aresult = mysqli_fetch_assoc($aquery);
            if(mysqli_num_rows($aquery) == 1) {
                $sentfrom = "<a href='".$aresult['applink']."'>".$tweetlinerow["sentfrom"]."</a>";
            } else {
                $sentfrom = $tweetlinerow["sentfrom"];
            }
            echo("
                <div class='$oddevenval publictimeline_tweet'>
                    <span class='user_actions'>
                    $favorite<br>
                    $usercontent
                    </span>
                    <span class='publictimeline_prof_wrapper'>
                        <span class='publictimeline_prof'>
                            <img width='100%' height='100%' src='/profiles/images/".$tweetlinerow["username"].".png'>
                        </span>
                    </span>
                    <span class='publictimeline_tweet_content'>
                        <b><a href='".$tweetlinerow["username"]."'>".$tweetlinerow["username"]."</a></b> <span> ".$tweetlinerow["tweet"]."</span>
                        <span class='timestamp'><a href='/statuses/".$tweetlinerow["id"]."''><span class='timeago' title='".$tweetlinerow["timestamp"]."".$globaluserinf['timezone']."'>".$tweetlinerow["timestamp"]."</span></a> from $sentfrom</span>
                </div>");
        }
    }
?>
</div>